/*
* Dominic Marques
* Flahavan Abbott
* Crystal Colon,ccolon2
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <stdbool.h>
#include <ctype.h>
#include <sys/time.h>

#define MAX_LINE 4096
#define streq(s0, s1)(strcmp((s0), (s1)) == 0)
#define trim(str)(str[strlen(str) - 1] = 0)

int main(int argc, char * argv[]) {
    FILE * fd;
    FILE * checksum_fp;
    struct hostent * hp;
    struct sockaddr_in sin;
    char * host;
    char filename[MAX_LINE];
    char buf[MAX_LINE];
    char buf2[MAX_LINE];
    char buf3[MAX_LINE];
    char ans[MAX_LINE];

    char buf6[MAX_LINE];
    int s, exists, success_val, file_length;
    int len, dir_name_length, dir_length;
    int i;
		float time;

		struct timeval tval_before, tval_after, tval_result;

    if (argc == 3) {
        host = argv[1];
    } else {
        fprintf(stderr, "usage: simplex-talk host\n");
        exit(1);
    }
    /* translate host name into peer's IP address */
    hp = gethostbyname(host);
    if (!hp) {
        fprintf(stderr, "simplex-talk: unknown host: %s\n", host);
        exit(1);
    }
    /* build address data structure */
    bzero((char * ) & sin, sizeof(sin));
    sin.sin_family = AF_INET;
    bcopy(hp -> h_addr, (char * ) & sin.sin_addr, hp -> h_length);
    sin.sin_port = htons(atoi(argv[2]));
    /* active open */
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }
    printf("Welcome to your first TCP client! To quit, type \'QUIT\'\n");
    if (connect(s, (struct sockaddr * ) & sin, sizeof(sin)) < 0) {
        perror("simplex-talk: connect");
        close(s);
        exit(1);
    }
    /* main loop: get and send lines of text */
    char * token, * delim = " ";
    char input[MAX_LINE];
    char command[100];
    int length, curlength, total_length;
		int32_t return_val = 0;
    unsigned char server_md5[33];
    unsigned char md5[33];

    while (fgets(input, sizeof(input), stdin)) {
        input[MAX_LINE - 1] = '\0';
        if (!isalpha(input[strlen(input) - 1])) {
            input[strlen(input) - 1] = '\0';

        }
        token = strtok(input, delim);
        if (!strncmp(token, "QUIT", 4)) {
            printf("Good Bye!\n");
            break;
        }
         bzero((char * ) & buf, sizeof(buf));
        strcpy(buf, token);
        /* sending sizeof(buf) bytes because that's what the server
           expecting. Otherwise, it will hang. Make sure to zero out each time*/
        if (send(s, buf, sizeof(buf), 0) == -1) {
            perror("client send error!");
            exit(1);
        }
        if (streq(token, "CD")) {
            token = strtok(NULL, delim);
            bzero((char * ) & buf, sizeof(buf));
            length = htons(strlen(token));
            sprintf(buf, "%d", length);
            if (send(s, buf, sizeof(buf), 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            strcpy(buf, token);
            if (send(s, buf, sizeof(buf), 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) {
                perror("Server Received Error!");
                exit(1);
            }
            switch ((int16_t) ntohs(atoi(buf))) {
            case -2:
                printf("The directory does not exist on server\n");
                break;
            case -1:
                printf("Error in changing directory.\n");
                break;
            case 1:
                printf("Changed current directory\n");
                break;
            default:
                printf("Error in changing directory.\n");
                break;
            }
        }
              else if (strcmp(token, "RM\0") == 0) {
            //Send RM commnad
            if (send(s, token, len, 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            //Calculate length of file name to be sent
            token = strtok(NULL, " ");
            // printf("piece: *%s*", token);

            file_length = strlen(token);
            file_length = htons(file_length);

            //Send file name length
            if (send(s, & file_length, sizeof(file_length), 0) == -1) { 
                perror("client send error!");
                exit(1);
            }
            file_length = ntohs(file_length);
        
            sprintf(buf3, "%s", token);
             if (send(s, buf3, file_length + 1, 0) == -1) {
                perror("client send error!");
                exit(1);
            }
           
            //Recv whether or not file exists
            if (recv(s, & exists, sizeof(exists), 0) == -1) {
                perror("Client Received Directory Size Error");
            }
            exists = ntohl(exists);
          
            if (exists < 0) {
                printf("File does not exist on the server\n");
              

            } else {
               
                printf("Are you sure you would like to delete the file?\n");
                fgets(ans, sizeof(ans), stdin);
            
                if (strcmp(ans, "yes\n\0") == 0) {
                         //send yes to server so that it can delete the file
                    if (send(s, ans, strlen(ans) + 1, 0) == -1) {
                        perror("client send error!");
                        exit(1);
                    }
                    //Receive whether or not delete was sucessful
                    if (recv(s, & success_val, sizeof(success_val), 0) == -1) {
                        perror("client receive success_val error");
                    }
                    success_val = ntohl(success_val);
                                  if (success_val == 1) {
                        printf("Deletion was successful\n");
                    } else {
                        printf("Deletion was unsuccessful\n");
                    }
                } else {


                    if (send(s, ans, strlen(ans) + 1, 0) == -1) {
                        perror("client send error!");
                        exit(1);
                    }
                    printf("delete abandoned\n");
                    continue;
                }
            }
        }
       

    
        else if (strcmp(input, "LS\0") == 0) {
          
            //Send LS command to server
            if (send(s, input, len, 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            //Receive current directory size
            if (recv(s, & dir_length, sizeof(dir_length), 0) == -1) {
                perror("Client Received Directory Size Error");
            }
            dir_length = ntohs(dir_length);
                      //Receive directory listings
            if (recv(s, buf2, dir_length, 0) == -1) {
                              perror("Client Received Directory Listing Error!");
            }
            
            printf(buf2);
            printf("\n");

        }
        else if (strcmp(token, "MKDIR\0") == 0) {
        
            //Send mkdir command to server

     
            if (send(s, token, len, 0) == -1) {
                perror("client send error!");
                exit(1);
            }

            token = strtok(NULL, " ");
         
            dir_name_length = htons(strlen(token));

            if (send(s, & dir_name_length, sizeof(dir_name_length), 0) == -1) { 
                perror("client send error!");
                exit(1);
            }

            dir_name_length = ntohs(dir_name_length);
            sprintf(buf6, "%s", token);

            buf6[dir_name_length] = '\0';

     
            if (send(s, buf6, dir_name_length + 1, 0) == -1) {
                perror("client send error!");
                exit(1);
            }

            //Recv whether or not directory was successfuly created
            if (recv(s, & exists, sizeof(exists), 0) == -1) {
                perror("Client Received Directory Size Error");
            }
            exists = ntohl(exists);
        

            if (exists == -2) {
                printf("The directory already exists on server\n");
                continue;
            } else if (exists == 1) {

                printf("The directory was successfully made\n");
            } else if (exists == -1) {
                printf("Error in making directory\n");
                continue;
            } else {

                printf("Other error occured \n");
            }
        } else if (streq(token, "RMDIR")) {
            token = strtok(NULL, delim);
            bzero((char*)&buf, sizeof(buf));
            length = htons(strlen(token));
            sprintf(buf, "%d", length);
            if (send(s, buf, sizeof(buf), 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            bzero((char*)&buf, sizeof(buf));
            strcpy(buf, token);
            if (send(s, buf, sizeof(buf), 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            bzero((char*)&buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) {
                perror("Server Received Error!");
                exit(1);
            }
            bool delete = false;
            if (((int16_t)ntohs(atoi(buf))) == -2) {
                printf("The directory is not empty.\n");
            } else if (((int16_t)ntohs(atoi(buf))) == -1) {
                printf("Error in changing directory.\n");
            } else if (((int16_t)ntohs(atoi(buf))) == 1) {
                printf("Are you sure you want to delete the directory?\nType \"Yes\" to confirm.\n");
                fgets(input, sizeof(input), stdin);
                delete = strncmp(input, "Yes", 3) == 0;
            }
            if (((int16_t)ntohs(atoi(buf))) != 1) continue;
            if (!delete) {
                printf("Failed to delete directory.\n");
                continue;
            }
            bzero((char*)&buf, sizeof(buf));
            strcpy(buf, input);
            if (send(s, buf, sizeof(buf), 0) == -1) {
                perror("client send error!");
                exit(1);
            }
            bzero((char*)&buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) {
                perror("Server Received Error!");
                exit(1);
            }
            switch ((int16_t)ntohs(atoi(buf))) {
                case 1:
                    printf("Directory deleted.\n");
                    break;
                case -1:
                    printf("Failed to delete directory.\n");
                    break;
            }
        } else if (streq(token, "HEAD")) {
            token = strtok(NULL, delim);
            bzero((char * ) & buf, sizeof(buf));
            length = htons(strlen(token));
            sprintf(buf, "%d", length);
            if (send(s, buf, sizeof(buf), 0) == -1) { // send length of file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            strcpy(buf, token);
            if (send(s, buf, sizeof(buf), 0) == -1) { // send file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) { // receive size of lines
                perror("Client received error!");
                exit(1);
            }
            length = htonl(atoi(buf));
            bzero((char * ) & buf, sizeof(buf));
            for (i = 0; i < 10; i++) { // loop to receive all ten lines of the head
                if ((len = recv(s, buf, sizeof(buf) + 1, 0)) == -1) {
                    perror("Client received error!");
                    exit(1);
                }
                printf("%s", buf);
                bzero((char * ) & buf, sizeof(buf));
            }
        } else if (streq(token, "DN")) {
            token = strtok(NULL, delim);
            bzero((char * ) & buf, sizeof(buf));
            length = htons(strlen(token));
            sprintf(buf, "%d", length);
            if (send(s, buf, sizeof(buf), 0) == -1) { // send length of file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            strcpy(buf, token);
            strcpy(filename, token);
            if (send(s, buf, sizeof(buf), 0) == -1) { // send file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) { // receive md5sum
                perror("Client received error!");
                exit(1);
            }
            strcpy(server_md5, buf);
            bzero((char *)&buf, sizeof(buf));
            if ((len = recv(s, buf, sizeof(buf), 0)) == -1) { // receive size of lines
                perror("Client received error!");
                exit(1);
            }
            length = ntohl(atoi(buf));
            if (length <= 0) {
                perror("Error opening file in the server");
                exit(1);
            }
						total_length = length;
            curlength = 0;
            bzero((char * ) & buf, sizeof(buf));
            fd = fopen(filename, "w+");
						gettimeofday(&tval_before, NULL);
            while (curlength < length) { // loop to receive all lines and write to file
                length -= curlength;
                if ((len = recv(s, buf, sizeof(buf), 0)) == -1) {
                    perror("Client received error!");
                    exit(1);
                }
                fprintf(fd, "%s", buf);
                curlength = strlen(buf);
                bzero((char * ) & buf, sizeof(buf));
            }
            fclose(fd);
						gettimeofday(&tval_after, NULL);
            sprintf(command, "md5sum %s", filename);
            checksum_fp = popen(command, "r");
            fgets(md5, sizeof(md5), checksum_fp);
            pclose(checksum_fp);
						timersub(&tval_after, &tval_before, &tval_result);
						sprintf(buf, "%ld.%06ld", (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
            printf("Bytes transferred: %d\n", total_length);
						printf("Time elapsed: %s seconds\n", buf);

						printf("Throughput: %.6f megabytes per second\n", total_length/(1000000*atof(buf)));
            if (streq(md5, server_md5)) {
                printf("The checksum of the new file matches the original. md5: %s\n", md5);
            } else {
                printf("The checksum of the new file does not match the original. NEW md5: %s\n", md5);
            }
						bzero((char*)&buf, sizeof(buf));
						bzero((char*)&md5, sizeof(md5));
						bzero((char*)&server_md5, sizeof(server_md5));
						bzero((char*)&filename, sizeof(filename));
        } else if(streq(token, "UP")){
            token = strtok(NULL, delim);
            bzero((char * ) & buf, sizeof(buf));
            length = htons(strlen(token));
            sprintf(buf, "%d", length);

            if (send(s, buf, sizeof(buf), 0) == -1) { // send length of file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
            strcpy(buf, token);
						strcpy(filename, token);
						// printf("File name: %s\n", buf);
            if (send(s, buf, sizeof(buf), 0) == -1) { // send file name
                perror("Client send error!");
                exit(1);
            }
            bzero((char * ) & buf, sizeof(buf));
						fd = fopen(filename, "r");
						if(fd == NULL){
								// file does not exist
								length = -1;
								sprintf(buf, "%d", htonl(length));
								if(send(s, buf, sizeof(buf), 0) == -1){
										perror("Client send error!");
										exit(1);
								}
						} else{
								// file exists
								while (fgets(buf, sizeof(buf), fd)) {
										return_val += strlen(buf);						// calc length of file
										bzero((char*)&buf, sizeof(buf));
								}
							    	sprintf(command, "md5sum %s", filename);
								checksum_fp = popen(command, "r");
								fgets(md5, sizeof(md5), checksum_fp); // get md5sum of file
								pclose(checksum_fp);
								rewind(fd);
								if((len = recv(s, buf, sizeof(buf), 0)) == -1){ // receive confirmation
										perror("Client recieve error!");
										exit(1);
								}
								if(strncmp(buf, "READY", 5)!=0){
										perror("Error on server side");
										exit(1);
								}
    								bzero((char*)&buf, sizeof(buf));
								sprintf(buf, "%d", htonl(length));
								if (send(s, buf, sizeof(buf), 0) == -1){ // send size of file
										perror("Client send error!");
										exit(1);
								}

								printf("%s\n", buf);
								bzero((char *)&buf, sizeof(buf));

								while(fgets(buf, sizeof(buf), fd)){                                                     										if(send(s, buf, sizeof(buf), 0) == -1){ // loop through to send whole file
												perror("Client send error!");
												exit(1);
										}
										bzero((char *)&buf, sizeof(buf));
								}

								if((len = recv(s, buf, sizeof(buf), 0)) == -1){ // receive elapsed time
										perror("Client receive error!");
										exit(1);
								}
								time = atof(buf);
								bzero((char*)&buf, sizeof(buf));

								if((len = recv(s, buf, sizeof(buf), 0)) == -1){ // receive md5sum
										perror("Client receive error!");
										exit(1);
								}
								strcpy(server_md5, buf);
								if (streq(md5, server_md5)) {
										printf("Bytes transferred: %d\n", total_length);
										printf("Time elapsed: %s seconds\n", buf);
										printf("Throughput: %.6f megabytes per second\n", length/(1000000*time));
								    printf("The checksum of the new file matches the original. md5: %s\n", md5);
								} else {
								    printf("The transfer failed: md5 hashes didn't match\n");
								}
								bzero((char*)&buf, sizeof(buf));
								bzero((char*)&md5, sizeof(md5));
								bzero((char*)&server_md5, sizeof(server_md5));
								bzero((char*)&filename, sizeof(filename));
								return_val = 0;
						}

				}

    }

    close(s);
    return 0;
}
