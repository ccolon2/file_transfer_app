/*
* Dominic Marques
* Flahavan Abbott
* Crystal Colon
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#include <stdbool.h>
#include <dirent.h>
#include <sys/time.h>

#define MAX_PENDING 5
#define MAX_LINE 4096
#define streq(s0, s1)(strcmp((s0), (s1)) == 0)

#define trim(str)(str[strlen(str) - 1] = 0)

void pwd() {
    char buffer[BUFSIZ];
    if (getcwd(buffer, BUFSIZ) == NULL) {
        fprintf(stderr, "Getcwd error: %s\n", strerror(errno));
        exit(errno);
    }
    printf("%s\n", buffer);
}

int changedir(char * path) {
    if (chdir(path) < 0) {
        fprintf(stderr, "Chdir error: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

bool isDirectoryEmpty(char * dirname) {
    int n = 0;
    struct dirent * d;
    DIR * dir = opendir(dirname);
    if (dir == NULL) //Not a directory or doesn't exist
        return 1;
    while ((d = readdir(dir)) != NULL) {
        if (++n > 2)
            break;
    }
    closedir(dir);
    return n <= 2; //Directory Empty
}

bool directoryExists(char * dirname) {
    DIR * dir = opendir(dirname);
    if (dir) {
        closedir(dir);
        return true;
    }
    return false;
}

int checkDir(char * dirname) {
    if (directoryExists(dirname)) {
        if (isDirectoryEmpty(dirname)) {
            return 1;
        } else {
            return -2;
        }
    } else {
        return -1;
    }
}

int main(int argc, char
    const * argv[]) {
    struct sockaddr_in sin, client_addr;
    char buf[MAX_LINE];
    int addr_len, new_s, i, len;
    char buf6[MAX_LINE];
    char buf3[MAX_LINE];
    char buf2[MAX_LINE];
    char buf4[MAX_LINE];
    char buf5[MAX_LINE];
    FILE * file;
    int s, file_len, DNE, exists, success_val;
    char cmd[] = "ls -l";
    int dir_len, len2;

		struct timeval tval_before, tval_after, tval_result;

    //int check;
    DIR * directory;
    int pos, neg, check;
    const void * opt;
    FILE * fd;
    FILE * checksum_fp;
    unsigned char md5[33];
    char command[100];
    //MD5_CTX mdContext;
    /*Check for correct arguments*/
    if (argc != 2) {
        printf("Usage: %s <port>\n", argv[0]);
        exit(0);
    }
    /* build address data structure */
    bzero((char * ) & sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(atoi(argv[1]));
    /* setup passive open */
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }
    // set socket option

    if ((setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char * ) & opt, sizeof(int))) < 0) {
        perror("simplex-talk:setscokt");
        exit(1);
    }
    if ((bind(s, (struct sockaddr * ) & sin, sizeof(sin))) < 0) {
        perror("simplex-talk: bind");
        exit(1);
    }
    if ((listen(s, MAX_PENDING)) < 0) {
        perror("simplex-talk: listen");
        exit(1);
    }
     printf("Waiting... \n");
    /* wait for connection, then receive and print text */
    addr_len = sizeof(client_addr);
    char * token, * delim = " ";
    int length, curlength, total_length;
    char * dir;
    char filename[MAX_LINE];
    while (1) {
       
        if ((new_s = accept(s, (struct sockaddr * ) & client_addr, & addr_len)) < 0) {
            perror("simplex-talk: accept");
            exit(1);
        }
       

        while (1) {
            // printf("BEFORE RECEIVE\n");
            if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                perror("Server Received Error!");
                exit(1);
            }
            // printf("Server Received:*%s*\n", buf);

            //LS command/////////////
            // printf("buf: *%s*\n", buf);

            if (len == 0 || strncmp(buf, "QUIT", 4) == 0) {
                break;
            } else if (strncmp(buf, "PWD", 3) == 0) {
                pwd();
            } else if (strncmp(buf, "CD", 2) == 0) {
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                    perror("Server Received Error!");
                    exit(1);
                }
                length = ntohs(atoi(buf));
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                    perror("Server Received Error!");
                    exit(1);
                }
                dir = strdup(buf);
                bzero((char * ) & buf, sizeof(buf));
                int16_t return_int;
                if (strlen(dir) == length) {
                    if (chdir(dir) < 0) {
                        return_int = -2;
                    } else {
                        return_int = 1;
                    }

                } else {
                    return_int = -1;
                }
                bzero((char * ) & buf, sizeof(buf));
                sprintf(buf, "%d", htons(return_int));
                if (send(new_s, buf, sizeof(buf), 0) == -1) {
                    perror("Error sending");
                    exit(1);
                }
            } else if (strcmp(buf, "RM\0") == 0) {
                               if (recv(new_s, & file_len, sizeof(file_len), 0) == -1) {
                    perror("Server Received file_length error");
                }
                file_len = ntohs(file_len);
                            if (recv(new_s, buf4, file_len + 1, 0) == -1) {
                    perror("Server received filename error");
                    exit(1);
                }
                              ////Check if the file to be deleted exists
              
                if (access((char * ) buf4, F_OK) != -1) {
                  
                    exists = 1;
                   
                    exists = htonl(exists);
                    //Send file exists 1  back to client
                    if (send(new_s, & exists, sizeof(exists), 0) == -1) {
                        perror("server send error!");
                        exit(1);
                    }
                  

                    if (recv(new_s, buf5, sizeof(buf5), 0) == -1) {
                        perror("Server received filename error");
                        exit(1);
                    }

                                 if (strcmp(buf5, "yes\n\0") == 0) {
                    
                        //delete the file
                        if (remove(buf4) == 0) {
                            //send back to client success
                          
                            success_val = htonl(1);
                            if (send(new_s, & success_val, sizeof(success_val), 0) == -1) {
                                perror("failed to send success value");
                            }
                        } else {
                            success_val = htonl(-1);
                            //send back to client failure
                            if (send(new_s, & success_val, sizeof(success_val), 0) == -1) {
                                perror("failed to send success value");
                            }
                        }
                    } else if (strcmp(buf5, "no\n\0") == 0) {
                    
                        continue;
                    }

                } else //DNE
                {
                    // printf("DNE: %d\n", DNE);
                    DNE = -1;
                    DNE = htonl(DNE);
                    if (send(new_s, & DNE, sizeof(DNE), 0) == -1) {
                        perror("server send error!");
                        exit(1);
                    }
                
                }
            } 

            else if (strcmp(buf, "LS\0") == 0) {
                // printf("in ls\n");
                file = popen(cmd, "r");

                bzero((char * ) buf3, sizeof(buf3));

                //bzero(buf3, sizeof(buf3));

                if (file) {
                    while (!feof(file)) {
                        if (fgets(buf2, MAX_LINE, file) != NULL) {
                            //printf(buf2);
                            strcat(buf3, buf2);
                        }
                    }
                    if (pclose(file) != 0) {
                        printf("Error using pclose");
                    }
                }
                length = strlen(buf3);

            
                length = htons(length);

                //Send length of directory listings

                if (send(new_s, & length, sizeof(length), 0) == -1) {
                    perror("server send error!");
                    exit(1);
                }
                length = ntohs(length);

                //Send directory listings
               
                if (send(new_s, buf3, sizeof(buf3), 0) == -1) {
                    perror("server send error!");
                    exit(1);
                }
           
            } else if (strcmp(buf, "MKDIR\0") == 0) {
                //Receive length of directory name
                if (recv(new_s, & dir_len, sizeof(dir_len), 0) == -1) {
                    perror("Server Received dir_length error");
                }
       
                dir_len = ntohs(dir_len);
               

                if ((len2 = recv(new_s, buf6, sizeof(buf6), 0)) == -1) {
                    perror("Receive dir name error");
                    exit(1);
                }
               
                //Try to open the directory to see if it exists
                directory = opendir(buf6);
                if (directory) {
                    //Directory exists
                    ////Send negative affirmation to client
                    neg = -2;
              
                    neg = htonl(neg);
                    if (send(new_s, & neg, sizeof(neg), 0) == -1) {
                        perror("server send confirmation error\n");
                    }
                    // printf("Directory already exists \n");
                } else if (ENOENT == errno) {
                    //directory doesn't exist
                    ////Make directory
                    check = mkdir(buf6, 0777);
                    if (!check) {
                     
                        //send back to client that the dir was created
                        pos = 1;
                   
                        pos = htonl(pos);
                        if (send(new_s, & pos, sizeof(pos), 0) == -1) {
                            perror("Server send confirmation error\n");
                        }
                    } else { 
                        // printf("Unable to created dir\n");
                        neg = -1;
                     
                        neg = htonl(neg);
                        if (send(new_s, & neg, sizeof(neg), 0) == -1) {
                            perror("server send confirmation error\n");
                        }
                    }

                }
            } else if (strncmp(buf, "RMDIR", 5) == 0) {
                bzero((char*)&buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                    perror("Server Received Error!");
                    exit(1);
                }
                length = ntohs(atoi(buf));
                bzero((char*)&buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                    perror("Server Received Error!");
                    exit(1);
                }
                dir = strdup(buf);
                bzero((char*)&buf, sizeof(buf));
                int16_t return_int;
                if (strlen(dir) == length) {
                    return_int = checkDir(dir);
                } else {
                    return_int = -1;
                }
                // send directory status
                bzero((char*)&buf, sizeof(buf));
                sprintf(buf, "%d", htons(return_int));
                if (send(new_s, buf, sizeof(buf), 0) == -1) {
                    perror("Error sending");
                    exit(1);
                }
                if (return_int < 0) continue;
                // recieve confirmation from client
                bzero((char*)&buf, sizeof(buf));
                while (strlen(buf) == 0) {
                    if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) {
                        perror("Server Received Error!");
                        exit(1);
                    }
                }
                if (strncmp(buf, "Yes", 3) == 0) {
                    return_int = rmdir(dir);
                }
                if (return_int == 0) return_int = 1;
                bzero((char*)&buf, sizeof(buf));
                sprintf(buf, "%d", htons(return_int));
                if (send(new_s, buf, sizeof(buf), 0) == -1) {
                    perror("Error sending");
                    exit(1);
                }
            } else if (strncmp(buf, "HEAD", 4) == 0) {
                int32_t return_val = 0;
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive lenth of filename
                    perror("Server Received Error!");
                    exit(1);
                }
                length = ntohs(atoi(buf));
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive file name
                    perror("Server Received Error!");
                    exit(1);
                }
                strcpy(filename, buf);
                bzero((char * ) & buf, sizeof(buf));
                fd = fopen(filename, "r");
                if (fd == NULL) {
                    //File does not exist
                    return_val = -1;
                    sprintf(buf, "%d", htonl(return_val));
                    if (send(new_s, buf, sizeof(buf), 0) == -1) {
                        perror("Server send error!");
                        exit(1);
                    }
                    bzero((char * ) & buf, sizeof(buf));
                } else {
                    // File exists
                    for (i = 0; i < 10; i++) { // read first ten lines to calc size
                        if (fgets(buf, sizeof(buf), fd) == NULL) {
                            perror("Error reading file!");
                            exit(1);
                        }
                        return_val += strlen(buf);
                        bzero((char * ) & buf, sizeof(buf));
                    }
                    rewind(fd);
                    sprintf(buf, "%d", htonl(return_val));
                    if (send(new_s, buf, sizeof(buf), 0) == -1) { // send size of first ten lines
                        perror("Server send error!");
                        exit(1);
                    }
                    bzero((char * ) & buf, sizeof(buf));
                    for (i = 0; i < 10; i++) {
                        if (fgets(buf, sizeof(buf), fd)) { // read first ten lines again and send
                            if (send(new_s, buf, strlen(buf) + 1, 0) == -1) {
                                perror("Server send error!");
                                exit(1);
                            }
                        }
                        bzero((char * ) & buf, sizeof(buf));
                    }
                    fclose(fd);
                }
            } else if (strncmp(buf, "DN", 2) == 0) {
                int32_t return_val = 0;
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive lenth of filename
                    perror("Server Received Error!");
                    exit(1);
                }
                length = ntohs(atoi(buf));
                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive file name
                    perror("Server Received Error!");
                    exit(1);
                }
                strcpy(filename, buf);
                bzero((char * ) & buf, sizeof(buf));
                fd = fopen(filename, "r");
                if (fd == NULL) {
                    //File does not exist
                    return_val = -1;
                    sprintf(buf, "%d", htonl(return_val));
                    if (send(new_s, buf, sizeof(buf), 0) == -1) {
                        perror("Server send error!");
                        exit(1);
                    }
                } else {
                    // File exists
                    while (fgets(buf, sizeof(buf), fd)) {
                        return_val += strlen(buf);
                        bzero((char * ) & buf, sizeof(buf));
                    }
                    sprintf(command, "md5sum %s", filename);
                    checksum_fp = popen(command, "r");
                    fgets(md5, sizeof(md5), checksum_fp);
                    pclose(checksum_fp);
										strcpy(buf, md5);
                    if (send(new_s, buf, sizeof(buf), 0) == -1) { // send md5sum
                        perror("Server send error!");
                        exit(1);
                    }
                    bzero((char *)&buf, sizeof(buf));
                    bzero((char * ) & md5, sizeof(md5));
                    rewind(fd);
                    sprintf(buf, "%d", htonl(return_val));
                    if (send(new_s, buf, sizeof(buf), 0) == -1) { // send size of file
                        perror("Server send error!");
                        exit(1);
                    }
                    bzero((char *)&buf, sizeof(buf));
                    while (fgets(buf, sizeof(buf), fd)) { // read lines again and send
                        if (send(new_s, buf, sizeof(buf), 0) == -1) {
                            perror("Server send error!");
                            exit(1);
                        }
                        bzero((char * ) & buf, sizeof(buf));
                    }
                    fclose(fd);
                }
            } else if (strncmp(buf, "UP", 2)==0){
							
								bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive lenth of filename
                    perror("Server Received Error!");
                    exit(1);
                }
                length = ntohs(atoi(buf));

                bzero((char * ) & buf, sizeof(buf));
                if ((len = recv(new_s, buf, sizeof(buf), 0)) == -1) { // receive file name
                    perror("Server Received Error!");
                    exit(1);
                }
                strcpy(filename, buf);
							
                bzero((char * ) & buf, sizeof(buf));
								sprintf(buf, "READY");
								if(send(new_s, buf, sizeof(buf), 0) == -1){ // send confirmation
										perror("Server send error!");
										exit(1);
								}
								bzero((char*)&buf, sizeof(buf));

								if((len = recv(new_s, buf, sizeof(buf), 0)) == -1){ // receive size of file
										perror("Server receive error!");
										exit(1);
								}
							
								length = ntohl(atoi(buf));
							
								bzero((char*)&buf, sizeof(buf));
								if (length <= 0){
										perror("Error opening file in the client");
										exit(1);
								}
								total_length = length;
								curlength = 0;
								bzero((char*)&buf, sizeof(buf));
							
								fd = fopen(filename, "w+");
								gettimeofday(&tval_before, NULL);
								
								while(curlength < length){ // loop to receive whole file
										length -= curlength;
										if((len = recv(new_s, buf, sizeof(buf), 0))==-1){
												perror("Server received error!");
												exit(1);
										}
                                        fprintf(fd, "%s", buf);
										curlength = strlen(buf);
										bzero((char*)&buf, sizeof(buf));
								}
								fclose(fd),
								gettimeofday(&tval_after, NULL);
								sprintf(command, "md5sum %s", filename);
								checksum_fp = popen(command, "r");
								fgets(md5, sizeof(md5), checksum_fp);
								pclose(checksum_fp);
								timersub(&tval_after, &tval_before, &tval_result);
								sprintf(buf, "%ld.%06ld", (long int)tval_result.tv_sec, (long int)tval_result.tv_usec);
								if(send(new_s, buf, sizeof(buf), 0) == -1){ 			// send elapsed time
										perror("Server send error");
										exit(1);
								}
								bzero((char*)&buf, sizeof(buf));
								if(send(new_s, md5, sizeof(md5), 0) == -1){ // send md5sum
										perror("Server send error");
										exit(1);
								}
								bzero((char*)&buf, sizeof(md5));
						}
        }
        printf("Client finishes, close the connection!\n");
        close(new_s);
    }
    close(s);
    return 0;
}
