included filenames: server.c,client.c,Makefile in server subdirectory,Makefile in client subdirectory

Example commmands

run server with ./server.out 41011

run client with ./client.out studentxx.cse.nd.edu 41011

type in LS to the client side

close it

make a delete.txt on server side

run client again and try to do "RM delete.txt"

restart client and server and run MKDIR puertorico on client side

type HEAD on client side to see first 10 lines of a file
type QUIT to exit the program
type CD to change directory
type DN to download a directory
type UP to upload
type MKDIR to create a directory
type RMDIR to remove a diretory
type RM to remove a file
type LS to list contents of directory on server



by Crystal Colon, Dominic Marques, and Flahavan Abbott
